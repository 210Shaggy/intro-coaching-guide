# Intro Coaching Guide

Coaching Guide that I've successfully used to help identify the strengths, motivations, and goals of team members. 

## What is this thing?

I have some specific questions to ask you so I can be more effective in supporting you and helping you achieve
your goals. I will ask the questions exactly as they are written. Although I won’t interpret the questions, I will
repeat a question at any time, if that is helpful. This interview includes some questions that will help me learn
more about your strengths, what you enjoy most about your work, what motivates you, what goals you’re focused
on, and your expectations of me.

Please be candid. Please be sure to answer what is right for you rather than what you think I might want to hear.

After the interview, we’ll set a time to talk further about the commitments I am prepared to make to ensure that you and your team are as successful as possible.
