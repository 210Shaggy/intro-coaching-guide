# Intro Coaching Questions

## Talent
1. What are your strengths? What do you do best?
2. What are the positive words people who know you well would use to describe you?
3. What are the positive words you would use to describe you?
4. Of the positive words that are used to describe you, which two do you feel best describe you?

> Talent Considerations [1-4]
> * How clear is this Zeeb about his or her strengths?
> * What similarities are there between this Zeeb’s perception of his or her strengths and what you see?
> * What could be done to further develop the two Signature Themes this Zeeb chose to focus on? If this Zeeb hasn’t taken Clifton Strengths Finder, consider other descriptors she or he used to describe her or his strengths. 

5. What do you enjoy most in your current role?
6. What strengths do you use in your current role?
7. Do you have strengths you think you could use more often?
8. Is there anything else you’d like to do that we have not talked about?

> Talent Considerations [5-8]
> * Have you seen this Zeeb demonstrate these strengths in their current role?
> * If yes, are there more ways they can use these strengths?
> * If no, have you observed strengths this Zeeb did not mention?

## Setting Expectations
1. What do you believe you get paid to do? How should we measure what you get paid to do?
2. What do you want to accomplish in the next month?
3. What do you want to accomplish in the next six months?

> Setting Expectations Considerations [1-3]
> * How does this Zeeb’s understanding of what they get paid to do compare with what you think?
> * How clear is this Zeeb about what they want to accomplish in the next month and in the next six months?  How realistic are those goals? How challenging are they?
> * How could you support this Zeeb in reaching their goal?

4. What do you think I expect of you this year? How do we determine whether or not you’re meeting these expectations?
5. What do you expect from me this year?

> Setting Expectations Considerations [4-5]
> * Does this Zeeb accurately understand what you expect?
> * Are this Zeeb’s expectations of you clear and realistic?
> * How can you help this Zeeb measure the outcomes of these expectations?

## Motivating
1. What do you find most satisfying about your work?
2. What about your work motivates you the most?
3. Tell me about the best recognition you have ever received. Why was it the best?

> Motivating Considerations [1-3]
> * How can you link this Zeeb’s motivators and satisfiers to recognition?
> * What strengths led to the achievement for which the associate received their best recognition?
> * What steps can you take to ensure that this Zeeb received recognition that honors his or her individual needs and expresses appreciation for their unique strengths?  

4. When you achieve your goals, how would you like to be recognized?
5. When you are successful, whom do you what to know it (think about people inside and outside of work)?

> Motivating Considerations [4-5]
> * How can you, as this Zeeb’s manager, make sure the people he or she mentioned know when they are successful?
> * What are some methods for communicating this Zeeb’s successes to their significant people?
> * How can you learn more about the significant people this Zeeb mentioned and the roles they place in supporting her or his efforts?

## Developing
1. Is there a particular skill you would like to acquire? If so, what’s the best way for you to acquire it?
2. Are there other strengths that you would like to build on?
3. How often do you think you and I should get together to discuss how things are going?

> Developing Considerations [1-3]
> * How does the skill mentioned relate to the Zeebs strengths and signature themes?
> * How can you help this Zeeb create opportunities to acquire the skills they have identified?
> * Will you be able to meet with this Zeeb as often as they would like? If not, how will you work with the individual to determine a mutually-acceptable schedule?

4. Whom would you like to learn from and what would you like to learn from her or him?
5. What is the best way for us to measure your progress? How can we track your development?
6. Is there anything else you would like to add that would help me know you better?

> Developing Considerations [4-6]
> * What can you do to help this Zeeb learn from the people they mentioned?
> * How do/could you define this Zeeb’s progress through clear numbers and measurement?
> * How can you help this Zeeb feel continually successful without experiencing a promotion? How could you create “levels” of achievement within his or her expected role?
> * What actions will you take to enhance this Zeeb’s productivity and engagement?
