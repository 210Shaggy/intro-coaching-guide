# Intro Coaching Questions

## Talent
1. What are your strengths? What do you do best?
2. What are the positive words people who know you well would use to describe you?
3. What are the positive words you would use to describe you?
4. Of the positive words that are used to describe you, which two do you feel best describe you?
5. What do you enjoy most in your current role?
6. What strengths do you use in your current role?
7. Do you have strengths you think you could use more often?
8. Is there anything else you’d like to do that we have not talked about?

## Setting Expectations
1. What do you believe you get paid to do? How should we measure what you get paid to do?
2. What do you want to accomplish in the next month?
3. What do you want to accomplish in the next six months?
4. What do you think I expect of you this year? How do we determine whether or not you’re meeting these expectations?
5. What do you expect from me this year?

## Motivating
1. What do you find most satisfying about your work?
2. What about your work motivates you the most?
3. Tell me about the best recognition you have ever received. Why was it the best?
4. When you achieve your goals, how would you like to be recognized?
5. When you are successful, whom do you what to know it (think about people inside and outside of work)?

## Developing
1. Is there a particular skill you would like to acquire? If so, what’s the best way for you to acquire it?
2. Are there other strengths that you would like to build on?
3. How often do you think you and I should get together to discuss how things are going?
4. Whom would you like to learn from and what would you like to learn from her or him?
5. What is the best way for us to measure your progress? How can we track your development?
6. Is there anything else you would like to add that would help me know you better?
